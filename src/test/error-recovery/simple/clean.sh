#!/usr/bin/env bash

rm -f original/formatted_*
rm -f original/*.cst
rm -f original/*.cst_symbols
rm -f original/*.tokens
rm -f original/*.errors
rm -rf recovered/
