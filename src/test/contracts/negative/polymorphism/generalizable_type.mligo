type _foo = | Foo of unit

let f (x : _foo) = x
